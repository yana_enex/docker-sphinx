* Uninstall old versions
```
# yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```
* Install using the repository
```
# yum install -y yum-utils
# yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```
* INSTALL DOCKER ENGINE
```
# yum install docker-ce docker-ce-cli containerd.io
```
* Start Docker.
```
# systemctl enable --now docker
```
* Install Docker Compose
* Run this command to download the current stable release of Docker Compose:
```
# curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```
* Apply executable permissions to the binary
```
# chmod +x /usr/local/bin/docker-compose
```
* Add your user to the docker group.
```
# usermod -aG docker bitrix
```
* Разрешить запуск docker пользователем bitrix
```
echo "bitrix ALL=(ALL) NOPASSWD: /usr/bin/docker" >> /etc/sudoers.d/bitrix
```
* Run container под пользователем bitrix
```
$ cd /home/bitrix/
$ git clone https://gitlab.com/yana_enex/docker-sphinx
$ cd sphinx 
$ docker-compose up -d
```
* Add crontask ```crontab -e ```
```
@reboot (while true; do sleep 5s; cd ~/sphinx; systemctl status network.target remote-fs.target && sudo docker-compose up -d && break; done)&
```