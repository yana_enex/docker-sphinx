FROM centos:7
RUN yum install -y http://sphinxsearch.com/files/sphinx-2.2.11-1.rhel7.x86_64.rpm
RUN mkdir /etc/sphinx/dicts
RUN curl -o /etc/sphinx/dicts/de.pak http://sphinxsearch.com/files/dicts/de.pak
RUN curl -o /etc/sphinx/dicts/en.pak http://sphinxsearch.com/files/dicts/en.pak
RUN curl -o /etc/sphinx/dicts/ru.pak http://sphinxsearch.com/files/dicts/ru.pak
COPY ./conf/sphinx.conf /etc/sphinx/sphinx.conf
ADD ./data /var/lib/sphinx
ENTRYPOINT ["/usr/bin/searchd", "--config", "/etc/sphinx/sphinx.conf", "--nodetach"]
